
export function updateFinishedTask(data) {
    return {
        type: "FINISHED_TASK",
        data,
    }
}